# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations


def index():
    content = []
    for c in [ [ "Chemicals",
                 # format: controller, function, name, table, arguments_if_any
                 [ "chemicals", "chemical", "Chemical", "chemicals_chemical"],
                 [ "default", "manage", "Chemical property type", "chemicals_tag_type" ],
                 [ "default", "manage", "Solution", "chemicals_solution" ],
                 [ "default", "manage", "Lot", "chemicals_lot" ],
                 [ "default", "manage", "Company", "chemicals_company" ],
                 [ "default", "manage", "Location", "chemicals_location" ],
                 [ "default", "manage", "SHOULD WE ADD CONSUMABLES?", "consumables" ] ],
               [ "Preparation",
                 [ "default", "manage", "Species", "prep_species" ],
                 [ "animals", "prep_mouse", "Preparation: Animal mouse", "prep_animal_mouse" ],
                 [ "default", "manage", "Preparation: Mouse cardiomyocytes", 'prep_cardiomyocyte_mouse' ],
                 [ "default", "manage_linked", "Preparation: Mouse muscles", 'prep_mouse_muscle',
                   'measurement_mouse_muscle_protein_value-measurement_mouse_muscle_ak_ck-measurement_mouse_muscle_citrate_synthase' +
                   '-measurement_mouse_muscle_hexokinase-measurement_mouse_muscle_ldh-measurement_mouse_muscle_pyruvate_kinase-kinetics_experiment_to_mouse_muscle'
                 ],
                 ],
               [ "Isolations",
                 [ "default", "manage", "Mouse cardiomyocytes", 'prep_proc_isolation_cardio_mouse'],
                 ],
               [ "Animals",
                 [ "default", "manage", "Mice", "animals_mouse" ],
                 [ "default", "manage", "Mice breeding", "animals_mouse_breeding_pair" ],
                 [ "default", "manage", "Mice litter", "animals_mouse_litter" ],
                 [ "default", "manage_ro", "Mice with age read only", "animals_mouse_age" ],
                 ],
               [ "Experiments",
                 [ "default", "manage_ro", "List of kinetics experiments, read only", "kinetics_experiment" ],
                 [ "default", "manage_ro", "List of kinetics experiments with extended details (mouse CM), read only", "kinetics_experiment_with_mouse" ],
                 [ "default", "manage", "Kinetics experiments linked to mouse cardiomyocytes", "kinetics_experiment_to_mouse_cardiomyocytes" ],
                 [ "default", "manage", "Electrophysiology: cells", "prep_electrophysiology_cardiomyocyte" ],
                 [ "default", "manage", "Electrophysiology: Ca fluxes via IV", "measurement_electrophysiology_cafluxes_iv" ],
                 [ "default", "manage", "Mechanics: carbon fibers", "prep_mechanics_carbonfiber"],
                 [ "default", "manage", "Mechanics: cells", "prep_mechanics_cardiomyocyte" ],
                 [ "default", "manage", "Mechanics: cardiomyocyte frequency response", "measurement_mechanics_ufreqcasl" ],
                 [ "default", "manage_ro", "Mechanics: unloaded cardiomyocyte frequency response extended, read only", "measurement_mechanics_ufreqcasl_ext" ],
                 [ "default", "manage", "Sparks experiments", "measurement_sparks" ],
                 [ "default", "manage_ro", "Sparks experiments info, read only", "sparks_experiment" ],
                 [ "default", "manage_ro", "Sparks experiments info with animal data, read only", "sparks_experiment_with_mouse" ],

                 [ "default", "manage", "Spectrophotometer: general experiment data", "kinetics_spectro_config" ],
                ],
               [ "Proteins",
                 [ "default", "manage", "Protein in mouse cardiomyocytes", "measurement_mouse_cardiomyocyte_protein" ],
                 [ "default", "manage_ro", "Protein in mouse cardiomyocytes results", "measurement_mouse_cardiomyocyte_protein_value" ],
                 [ "default", "manage", "Protein in mouse muscles", "measurement_mouse_muscle_protein" ],
                 [ "default", "manage_ro", "Protein in mouse muscles results", "measurement_mouse_muscle_protein_value" ],
                 [ "default", "manage_linked", "Protein measurement batches", "measurement_protein_batch", 'measurement_protein_spectrum_280_330nm' ],
                 [ "default", "manage_ro", "Protein measurement batches results", "measurement_protein_batch_content" ],
                 [ "default", "manage_ro", "Protein measurement samples read only", "measurement_protein_content_280_330nm" ],
                ],
               [ "Enzymes",
                 [ "default", "manage", "Cardiomyocytes: Citrate Synthase (CS)", "measurement_mouse_cardiomyocyte_citrate_synthase"],
                 [ "default", "manage_ro", "Cardiomyocytes: Citrate Synthase (CS) rate", "measurement_mouse_cardiomyocyte_citrate_synthase_rate" ],
                 [ "default", "manage_ro", "Cardiomyocytes: Citrate Synthase (CS) rate normalized by protein", "measurement_mouse_cardiomyocyte_citrate_synthase_rate_protein" ],

                 [ "default", "manage", "Mouse muscle: Adenylate Kinase and Creatine Kinase (AK+CK)", "measurement_mouse_muscle_ak_ck"],
                 [ "default", "manage_ro", "Mouse muscle: Adenylate Kinase and Creatine Kinase (AK+CK) rate", "measurement_mouse_muscle_ak_ck_rate" ],
                 [ "default", "manage_ro", "Mouse muscle: Adenylate Kinase and Creatine Kinase (AK+CK) rate normalized by protein", "measurement_mouse_muscle_ak_ck_rate_protein" ],
                 [ "default", "manage_ro", "Mouse muscle: Adenylate Kinase and Creatine Kinase (AK+CK) rate normalized by wet weight", "measurement_mouse_muscle_ak_ck_rate_ww"],

                 [ "default", "manage", "Mouse muscle: Citrate Synthase (CS)", "measurement_mouse_muscle_citrate_synthase" ],
                 [ "default", "manage_ro", "Mouse muscle: Citrate Synthase (CS) rate", "measurement_mouse_muscle_citrate_synthase_rate" ],
                 [ "default", "manage_ro", "Mouse muscle: Citrate Synthase (CS) rate normalized by protein", "measurement_mouse_muscle_citrate_synthase_rate_protein" ],
                 [ "default", "manage_ro", "Mouse muscle: Citrate Synthase (CS) rate normalized by wet weight", "measurement_mouse_muscle_citrate_synthase_rate_ww" ],

                 [ "default", "manage", "Mouse muscle: Cytochrome Oxidase (CytOx)", "measurement_mouse_muscle_cytochrome_oxidase" ],
                 [ "default", "manage_ro", "Mouse muscle: Cytochrome Oxidase (CytOx) rate constant normalized by wet weight", "measurement_mouse_muscle_cytochrome_oxidase_rate_constant_ww" ],

                 [ "default", "manage", "Mouse muscle: Hexokinase (HK)", "measurement_mouse_muscle_hexokinase" ],
                 [ "default", "manage_ro", "Mouse muscle: Hexokinase (HK) rate", "measurement_mouse_muscle_hexokinase_rate" ],
                 [ "default", "manage_ro", "Mouse muscle: Hexokinase (HK) rate normalized by protein", "measurement_mouse_muscle_hexokinase_rate_protein" ],

                 [ "default", "manage", "Mouse muscle: Lactate Dehydrogenase (LDH)", "measurement_mouse_muscle_ldh" ],
                 [ "default", "manage_ro", "Mouse muscle: Lactate Dehydrogenase (LDH) rate", "measurement_mouse_muscle_ldh_rate" ],
                 [ "default", "manage_ro", "Mouse muscle: Lactate Dehydrogenase (LDH) rate normalized by protein", "measurement_mouse_muscle_ldh_rate_protein" ],
                 [ "default", "manage_ro", "Mouse muscle: Lactate Dehydrogenase (LDH) rate normalized by wet weight", "measurement_mouse_muscle_ldh_rate_ww" ],

                 [ "default", "manage", "Mouse muscle: Pyruvate Kinase (PK)", "measurement_mouse_muscle_pyruvate_kinase" ],
                 [ "default", "manage_ro", "Mouse muscle: Pyruvate Kinase (PK) rate", "measurement_mouse_muscle_pyruvate_kinase_rate" ],
                 [ "default", "manage_ro", "Mouse muscle: Pyruvate Kinase (PK) rate normalized by protein", "measurement_mouse_muscle_pyruvate_kinase_rate_protein" ],
                 [ "default", "manage_ro", "Mouse muscle: Pyruvate Kinase (PK) rate normalized by wet weight", "measurement_mouse_muscle_pyruvate_kinase_rate_ww" ],

                ],
               [ "Respiration",
                 [ "default", "manage_ro", "Mouse cardiomyocytes: VO2(ADP) kinetics", "kinetics_vo2_adp_titration_mm_normalized" ],
                 [ "default", "manage_ro", "Mouse cardiomyocytes: VO2(ATP) kinetics", "kinetics_vo2_atp_titration_mm_normalized" ],
                 [ "default", "manage_ro", "Mouse cardiomyocytes: VO2 inhibition by PK and PEP", "kinetics_vo2_mervipk_ratios_normalized_extended" ],
                ],
               [ "Studies",
                 #[ "default", "manage", "Study of respiration changes due to aging on GAMT KO WT (Lucia)", "study_respiration_aging_gamt_lucia"],
                 [ "default", "manage", "Study of intracellular compartmentation in AGAT CM", "study_compartmentation_agat_cm_mouse"],
                 [ "default", "manage", "Study Ca sparks and transients in AGAT CMs", 'study_sparks_agat'],
                 [ "default", "manage", "Study Ca sparks and transients in GAMT CMs", 'study_sparks_gamt'],
                 [ "default", "manage", "Fractination study of HK AK CK", "study_fractionation_hkakck_mouse"],
                 [ "default", "manage", "Respiration study of HK AK CK", "study_respiration_hkakck_mouse"],
                 [ "default", "manage", "AGAT GAMT Skeletal muscle enzyme activities", "study_agat_gamt_skeletal"],
                 #[ "default", "manage", "Respiration study of C57 male female", "study_respiration_c57malefemale_mouse"],
                 #[ "default", "manage", "Study for sparks software paper showing ISO effect", "study_sparks_demo_iso"],
                 #[ "default", "manage", "Maanus MSc on Ca sparks and transients", "study_maanus_msc"],
                ],
               [ "Misc",
                 [ "default", "manage", "Muscle types", "muscle_type"],
                 [ "default", "manage", "Fraction types", "fraction_type"],
                 [ "default", "manage", "Units", "unit"] ]
               ]:
        cc = c[0]
        tup = []
        for i in c[1:]:
            arguments = [c[0], i[2]]
            arguments.extend(i[3:])
            tup.append( A(i[2], _href=URL(c=i[0], f=i[1], args=arguments)) )

        content.append( [c[0], UL(tuple(tup))] )

    content.append( ["User administration",
                     UL( A("Administer users", _href=URL(c="appadmin", f="manage", args=["auth"])) ) ] )

    return dict(message=T('Welcome to Laboratory of Systems Biology database'),
                content=content)

def manage():
    title = request.args(0).replace('_',' ')
    subtitle = request.args(1).replace('_',' ')
    table = request.args(2)
    if not table in db.tables(): redirect(URLerror)
    return dict(title=title, subtitle=subtitle,
                grid = SQLFORM.smartgrid(db[table],
                                         maxtextlength=maxlength_default,
                                         deletable=False,
                                         args=request.args[:3]))

def manage_del():
    title = request.args(0).replace('_',' ')
    subtitle = request.args(1).replace('_',' ')
    table = request.args(2)
    if not table in db.tables(): redirect(URLerror)
    return dict(title=title, subtitle=subtitle,
                grid = SQLFORM.smartgrid(db[table],
                                         maxtextlength=maxlength_default,
                                         deletable=True,
                                         args=request.args[:3]))

def manage_links_separate():
    title = request.args(0).replace('_',' ')
    subtitle = request.args(1).replace('_',' ')
    table = request.args(2)
    if not table in db.tables(): redirect(URLerror)
    return dict(title=title, subtitle=subtitle,
                grid = SQLFORM.smartgrid(db[table],
                                         maxtextlength=maxlength_default,
                                         deletable=False,
                                         links_in_grid=False,
                                         args=request.args[:3]))

def manage_linked():
    title = request.args(0).replace('_',' ')
    subtitle = request.args(1).replace('_',' ')
    table = request.args(2)
    linked = request.args(3).split('-')
    if not table in db.tables(): redirect(URLerror)
    return dict(title=title, subtitle=subtitle,
                grid = SQLFORM.smartgrid(db[table],
                                         maxtextlength=maxlength_default,
                                         linked_tables = linked,
                                         deletable=False,
                                         args=request.args[:4]))

def manage_ro():
    title = request.args(0).replace('_',' ')
    subtitle = request.args(1).replace('_',' ')
    table = request.args(2)
    if not table in db.tables(): redirect(URLerror)
    return dict(title=title, subtitle=subtitle,
                grid = SQLFORM.smartgrid(db[table],
                                         maxtextlength=maxlength_default,
                                         deletable=False,
                                         editable=False,
                                         args=request.args[:3]))

# def show():
#     title = request.args(0)
#     subtitle = request.args(1)
#     table = request.args(2)
#     if not table in db.tables(): redirect(URLerror)
#     return dict(title=title, subtitle=subtitle,
#                 data = db(db[table]).select())

def show():
    title = request.args(0)
    subtitle = request.args(1)
    table = request.args(2)
    if not table in db.tables(): redirect(URLerror)
    return dict(title=title, subtitle=subtitle,
                grid = SQLFORM.smartgrid(db[table],
                                         maxtextlength=maxlength_default,
                                         deletable=False,
                                         editable=False,
                                         args=request.args[:3]))

def display():
    table = request.args(0)
    element_id = request.args(1)
    if not table in db.tables(): redirect(URLerror)
    record = db[table][element_id]
    if record is None: redirect(URLerror)
    return dict(title=table.replace('_', ' ').capitalize(),
                elementid=element_id,
                form=SQLFORM(db[table], record, deletable=False))

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

def error():
    return "Error responding to your request"
