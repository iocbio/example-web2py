
def mice_with_age():
    return dict(title="Animals", subtitle="Mice with the extended form",
                grid = SQLFORM.grid(db(db.animals_mouse).select()))


                # SQLFORM.grid(db.animals_mouse.breeding==db.animals_mouse_breeding.id,
                #                     maxtextlength=maxlength_default))

def prep_mouse():
    title = request.args(0).replace('_',' ')
    subtitle = request.args(1).replace('_',' ')
    table = request.args(2)
    if not table in db.tables(): redirect(URLerror)
    return dict(title=title, subtitle=subtitle,
                grid = SQLFORM.smartgrid(db[table],
                                         maxtextlength=maxlength_default,
                                         deletable=False,
                                         links=[ dict(header='Muscles',
                                                      body=lambda row: A('Muscles', _href=URL(c='mouse_muscle_weights', f='insert', args=[row.id])) ) ],
                                         linked_tables=['prep_proc_isolation_cardio_mouse'],
                                         args=request.args[:3]))
