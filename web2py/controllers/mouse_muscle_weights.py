
def insert():
    prep_id = request.args(0,cast=int,otherwise=URLerror)
    r = db(db.prep_animal_mouse.id == prep_id).select().first()
    animal = r.animal

    muscles = []
    f2muscle_id = {}
    for m_types in db(db.muscle_type).select():
        name = m_types.name
        weight = None
        for w in db((db.prep_mouse_muscle.mouse==prep_id) & (db.prep_mouse_muscle.muscle==m_types.id)).select():
            weight = w.weight
        muscles.append([name, m_types.id, weight])
    muscles.sort()
    fields = []
    for m in muscles:
        name, tid, weight = m[0], m[1], m[2]
        fname = name.replace(' ', '_')
        f2muscle_id[fname] = tid
        fields.append(Field(fname,
                            type='double',
                            label=name,
                            comment = 'Weight [g]',
                            default=weight))

    form = SQLFORM.factory(*fields)
    if form.process().accepted:
        d = dict(form.vars)
        s = ''
        for k in d:
            if isinstance(d[k], float):
                s += k + " "
                db.prep_mouse_muscle.update_or_insert( (db.prep_mouse_muscle.mouse==prep_id) & (db.prep_mouse_muscle.muscle==f2muscle_id[k]),
                                                      mouse = prep_id,
                                                      muscle = f2muscle_id[k],
                                                      weight = d[k] )

        response.flash = 'Values accepted for ' + str(prep_id) + ": " + s
        redirect(URL(c='default', f='manage', args=["Preparation", "Preparation: Mouse muscles", 'prep_mouse_muscle']))

    return dict(title = "Muscle weights",
                prep_id = prep_id,
                ready = str(r.ready),
                anim_id = animal,
                form = form )
