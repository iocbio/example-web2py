#!/bin/bash

for i in controllers models views static; do
    rsync -av $i www-data@sysbio-db:/var/www/web2py/applications/experiments
done
