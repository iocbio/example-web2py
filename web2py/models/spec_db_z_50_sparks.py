# This must be executed after db.py and spec_db_prep. For that, it should
# be named so that alphabetical order is after those files

db.define_table('sparks_experiment',
                Field('experiment_id', type='text'),
                Field('filename', type='text'),
                Field('comment', type='text'),
                primarykey=['experiment_id'],
                migrate=False)

db.define_table('measurement_sparks',
                Field('preparation', type='reference prep_cardiomyocyte_mouse', notnull=True),
                Field('experiment', type='text', notnull=True, unique=True),
                Field('success', type='boolean', notnull=True, default=True, comment='Overall success'),
                Field('comment', type='text', label='Comment'),
                format = lambda r: str(r.id) + " ExpLink / " + str(r.preparation.id) + " PrepID",
                singular="Sparks experiment", plural="Sparks experiments")

def sparks_info(experiment_id):
    info = ''

    for row in db(db.sparks_experiment.experiment_id == experiment_id).select():
        info += '/'.join(row.filename.split('/')[-3:])

    return info


db.measurement_sparks.info = Field.Virtual('info',
                                           lambda row: sparks_info(row.measurement_sparks.experiment))

# view
db.define_table('sparks_experiment_with_mouse',
                Field('experiment_id'),
                Field('filename', type='text'),
                Field('comment', type='text'),
                Field('success', type='boolean'),
                Field('cm_preparation', label='CM preparation', type='reference prep_cardiomyocyte_mouse'),
                Field('cm_ready', label='Cells ready', type="datetime"),
                Field('anim_id', label='Animal ID', type='reference animals_mouse'),
                Field('sex'),
                Field('spid', label='Phenotype'),
                Field('age', type='double'),
                migrate=False,
                singular="Sparks experiment mouse CM", plural="Sparks experiments mouse CM")
