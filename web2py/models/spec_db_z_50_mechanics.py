# This must be executed after db.py and spec_db_prep. For that, it should
# be named so that alphabetical order is after those files

db.define_table('prep_mechanics_carbonfiber',
                Field('fiber_id', type='text', notnull=True, unique=True),
                Field('length', type='double', label='Length [mm]'),
                Field('stiffness', type='double', label='Stiffness [uN/um]'),
                Field('available', type='boolean', default=True, label='Available for experiments'),
                Field('owner', type='text', label='Owner of the fiber'),
                Field('comment', type='text', label='Comment'),
                format = lambda r: str(r.id) + " / " + str(r.fiber_id) + " " + str(r.owner),
                singular="Fibers used in mechanics experiments", plural="Fibers used in mechanics")

db.define_table('prep_mechanics_cardiomyocyte',
                Field('cell_id', type='text', notnull=True, unique=True),
                Field('preparation', type='reference prep_cardiomyocyte_mouse', notnull=True),
                Field('left_fiber', type='reference prep_mechanics_carbonfiber'),
                Field('right_fiber', type='reference prep_mechanics_carbonfiber'),
                Field('comment', type='text', label='Comment'),
                Field('labeling_solution', type='reference chemicals_solution', notnull=True, label='Solution used to label the cells. For example, Fluo4'),
                Field('owner', type='text'),
                Field('temperature', type='double', label='Temperature, C'),
                Field('width', type='double', label='Width [um]'),
                format = lambda r: str(r.id) + " / " + str(r.preparation.id) + " " + str(r.preparation.ready) + " / " + str(r.preparation.isolation.preparation_animal.animal.id),
                singular="Cell used in mechanics experiments", plural="Cells used in mechanics")

db.define_table('measurement_mechanics_ufreqcasl',
                Field('cell', type='reference prep_mechanics_cardiomyocyte',
                      comment='Cell ID / Cardiomyocyte preparation ID / Animal ID'),
                Field('experiment', type='text', notnull=True, comment='Reference to the experiment id'),
                Field('success', type='boolean', notnull=True, comment='Overall success'),
                Field('use_for_analysis', type='boolean', default=True, notnull=True, comment='Use for analysis if successful'),
                Field('sl', type='boolean', notnull=True, comment='Sarcomere length'),
                Field('ca_bound', type='boolean', notnull=True, comment='Bound calcium'),
                Field('ca_free', type='boolean', notnull=True, comment='Free calcium'),
                Field('fiber_left', type='boolean', default=False, notnull=True, comment='Left fiber deformation'),
                Field('fiber_right', type='boolean', default=False, notnull=True, comment='Right fiber deformation'),
                Field('kill', type='boolean', notnull=True, comment='Fluorescence estimated at high calcium measurement'),
                Field('fiber', type='boolean', comment='Fibers attached'),
                Field('stretch', type='boolean', comment='Stretched'),
                Field('solution', type='reference chemicals_solution', comment='Used solution'),
                Field('comment', type='text', label='Comment'),
                singular="Mechanics unloaded freq", plural="Mechanics unloaded freq")

# read only: used only to create virtual field
db.define_table('kinetics_mechanics',
                Field('experiment_id'),
                Field('filename'),
                Field('mode'),
                Field('title'),
                primarykey=['experiment_id'],
                migrate=False)

def km_info(experiment_id):
    info = ''

    for row in db(db.kinetics_experiment.experiment_id == experiment_id).select():
        info += row.date + ": "

    for row in db(db.kinetics_mechanics.experiment_id == experiment_id).select(orderby=db.kinetics_mechanics.title):
        info += row.title + ", "
    if len(info) > 2: info = info[:-2]

    return info

db.measurement_mechanics_ufreqcasl.info = Field.Virtual('info',
                                                         lambda row: km_info(row.measurement_mechanics_ufreqcasl.experiment))

# view
db.define_table('measurement_mechanics_ufreqcasl_ext',
                Field('experiment', type='text'),
                Field('success', type='boolean'),
                Field('use_for_analysis', type='boolean', default=True, notnull=True, comment='Use for analysis if successful'),
                Field('sl', type='boolean'),
                Field('ca_bound', type='boolean'),
                Field('ca_free', type='boolean'),
                Field('kill', type='boolean'),
                Field('comment', type='text', label='Comment'),
                Field('date'),
                Field('exp_time'),
                Field('title'),
                migrate=False)
