# This must be executed after db.py and spec_db_a. For that, it should
# be named so that alphabetical order is after those files

from datetime import datetime

db.define_table('prep_species',
                Field('spid', label="Species ID", required=True, notnull=True, unique=True,
                      comment="Unique identifier of species batch. For example, trout bought on some date from the farm"),
                Field('sptype', label="Type", comment="Generic type that can be used to pull data from the same animal type. For example, trout"),
                Field('origin'),
                Field('contact'),
                Field('available_from', type='date', required=True, notnull=True, comment="Availibility of this batch"),
                Field('available_till', type='date', comment="Availibility of this batch"),
                Field('comments'),
                format = lambda r: le('prep_species', r.id, r.spid),
                singular="Species or cell lines", plural="Species or cell lines")

def mousetxt(r):
    if r.species is None: spid = "Not specified"
    else: spid = r.species.spid
    return le('animals_mouse', r.id, str(r.id) + " / " + spid)
    #return le('animals_mouse', r.id, str(r.id) + " / " + str(r.species) )

db.define_table('animals_mouse_breeding_pair',
                Field('comments'),
                format = lambda r: le('animals_mouse_breeding_pair', r.id, r.id),
                singular="Mouse breeding pair", plural="Mouse breeding pairs")

db.define_table('animals_mouse_litter',
                Field('pair', type='reference animals_mouse_breeding_pair'),
                Field('cage', type='integer'),
                Field('birth', type='date'),
                Field('success', type='boolean'),
                Field('comments'),
                format = lambda r: le('animals_mouse_litter', r.id,
                                      str(r.birth) + " / " + str(r.success) + " / " +
                                      str(r.id) ),
                singular="Mouse litter", plural="Mouse litter")

db.define_table('animals_mouse',
                Field('earmark', type="integer", label="Ear mark"),
                Field('sex', type="string", requires=IS_IN_SET(["Male", "Female"])),
                Field('species', type='reference prep_species',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, "prep_species.id",
                                                    '%(spid)s'
                      ))),
                Field('litter', type="reference animals_mouse_litter"),
                Field('cage', type="integer", label="Cage number"),
                Field('death', type='date'),
                Field('comments'),
                format = mousetxt, # this is slow
                singular="Mouse", plural="Mice")
db.animals_mouse.litter.requires = IS_IN_DB(db, "animals_mouse_litter.id", db.animals_mouse_litter._format,
                                            orderby=~db.animals_mouse_litter.id)

db.define_table('animals_mouse_breeding_pair_parent',
                Field("pair", type='reference animals_mouse_breeding_pair'),
                Field("parent", type='reference animals_mouse'),
                #format = breeding_detail,
                singular="Mouse parent", plural="Mouse parents")

##############################################################################
# virtual fields that help to display the data
db.animals_mouse.used_as_parent = Field.Virtual('used_as_parent',
                                                lambda row: db( (row.animals_mouse.id==db.animals_mouse_breeding_pair_parent.parent) ).count())

# litter table virtual fields
def parents_ids(pair_id):
    s = ""
    for row in db(db.animals_mouse_breeding_pair_parent.pair == pair_id).select(join=db.animals_mouse.on(db.animals_mouse_breeding_pair_parent.parent==db.animals_mouse.id),
                                                                                orderby=db.animals_mouse.sex | db.animals_mouse.id):
        s += str(row.animals_mouse_breeding_pair_parent.parent) + " (" + row.animals_mouse.sex + "), "
    return s[:-2]

def offsprings_ids(litter_id):
    s = ""
    for sex in ["Female", "Male"]:
        r = ""
        for row in db( (db.animals_mouse.litter == litter_id) & (db.animals_mouse.sex==sex) ).select(orderby=db.animals_mouse.id):
            r += str(row.id) + ", "
        if len(r) > 0:
            s += r[:-2] + " (%s), " % sex
    if len(s) > 0:
        return s[:-2]
    return s

db.animals_mouse_litter.parents = Field.Virtual('parents',
                                                lambda row: parents_ids(row.animals_mouse_litter.pair))

db.animals_mouse_litter.offsprings = Field.Virtual('offsprings',
                                                   lambda row: offsprings_ids(row.animals_mouse_litter.id))

# ## example on how to do that with views
# db.define_table('animals_mouse_extended_test',
#                 #Field('id', type='reference animals_mouse'),
#                 Field('earmark', type="integer", label="Ear mark"),
#                 Field('sex'),
#                 Field('species', type='reference prep_species'),
#                 Field('birth', type='date'),
#                 Field('mother', type='reference animals_mouse',
#                       requires=IS_EMPTY_OR(IS_IN_DB(db, "animals_mouse.id", mousetxt))),
#                 Field('father', type='reference animals_mouse',
#                       requires=IS_EMPTY_OR(IS_IN_DB(db, "animals_mouse.id", mousetxt))),
#                 Field('death', type='date'),
#                 Field('comments'),
#                 migrate=False)

# ## example on how to do that with views
db.define_table('animals_mouse_age',
                Field('id', type='reference animals_mouse'),
                Field('earmark', type="integer", label="Ear mark"),
                Field('sex'),
                Field('spid'),
                Field('cage', type="integer"),
                Field('age', type='double'),
                Field('birth', type='date'),
                Field('death', type='date'),
                Field('litter', type='reference animals_mouse_litter'),
                Field('comments'),
                migrate=False
)


###############################################################################################
## Preparations
# db.define_table('prep_preparation_type',
#                 Field("name", required=True, notnull=True, unique=True),
#                 format = '%(name)s', singular="Preparation type", plural="Preparation types")
#
# db.define_table('prep_preparation',
#                 Field("prep_type", type="reference prep_preparation_type", label="Type"),
#                 Field("ready", type="datetime", default=datetime.now, label="Preparation prepared", comment="Time at which preparation was ready"),
#                 format = lambda r: r.prep_type.name + " / " + str(r.ready) + " / " + str(r.id), #"%(prep_type.name)s / %(ready)s / %(id)s",
#                 singular="Preparation", plural="Preparations")

# note that while web2py makes separate id field, we should use preparation field as a key
db.define_table('prep_animal_mouse',
                Field('animal', type='reference animals_mouse', unique=True, notnull=True),
                Field("ready", type="datetime", notnull=True, default=datetime.now, label="Preparation prepared", comment="Time at which preparation was ready"),
                Field('weight', type="double", comment="Weight of animal [g]"),
                Field('appearance', comment="Appearance of an animal"),
                Field('tibial_length', type="double", comment="Tibial length [cm]"),
                Field('comments'),
                #format=lambda r: str(r.animal.id) + " / " + str(r.ready),
                format=lambda r: le('prep_animal_mouse', r.id, str(r.animal.id) + " / " + str(r.ready)),
                singular="Preparation: mouse", plural="Preparations: mice")

db.define_table('prep_proc_isolation_cardio_mouse',
                Field('started', type="datetime", default=datetime.now, label="Isolation started", comment="Time at which you started isolation"),
                Field('preparation_animal', type='reference prep_animal_mouse', unique=True, notnull=True,
                      comment="Animal used as a source for isolation"#,
                      #requires=IS_IN_DB(db, "prep_animal_mouse.id", "%(id)s",
                      #                  _and = IS_NOT_IN_DB(db, "prep_proc_isolation_cardio_mouse.preparation_animal") ),
                      ),
                Field('thermostat_temperature', type="double", default=38.5, comment="Thermostat temperature during isolation [C]"),
                Field('initial_pressure', type="double", comment="Initial pressure [mmHg]"),
                Field('initial_flow', type="double", comment="Initial flow [ml/min]"),
                Field('post_incubation_time', type="double", comment="Time in post-incubation [min]"),
                Field('post_incubation_temperature', type="double", comment="Temperature during post-incubation [C]"),
                Field('comments'),
                format="%(id)s / %(started)s",
                singular="CM isolation", plural="CM isolations")

db.define_table('prep_proc_isolation_cardio_mouse_pump',
                Field("isolation", type='reference prep_proc_isolation_cardio_mouse', notnull=True),
                Field("time_moment", type="double",  notnull=True, comment="Time from start of experiments [min]"),
                Field("pump_flow", type="double", notnull=True, comment="Pump flow [ml/min]"),
                singular="Pump", plural="Pump")

db.define_table('prep_proc_isolation_cardio_mouse_pressure',
                Field("isolation", type='reference prep_proc_isolation_cardio_mouse', notnull=True),
                Field("time_moment", type="double",  notnull=True, comment="Time from start of experiments [min]"),
                Field("pressure", type="double", notnull=True, comment="Pressure [mmHg]"),
                singular="Pressure", plural="Pressure")

db.define_table('prep_proc_isolation_cardio_mouse_solution',
                Field("isolation", type='reference prep_proc_isolation_cardio_mouse', notnull=True),
                Field("time_moment", type="double",  notnull=True, comment="Time from start of experiments [min]"),
                Field("solution", type='reference chemicals_solution', notnull=True),
                Field("volume", type="double", notnull=True, comment="Added amount of solution [ml]"),
                Field('comments'),
                singular="Solution", plural="Solutions")
