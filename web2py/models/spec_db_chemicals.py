# This must be executed after db.py and spec_db_a. For that, it should
# be named so that alphabetical order is after those files

db.define_table('chemicals_chemical',
                Field('name', required=True, notnull=True, unique=True),
                Field('formula'),
                Field('aliases', type="list:string"),
                format = '%(name)s', singular="Chemical", plural="Chemicals")

db.define_table('chemicals_tag_type',
                Field('name', required=True, notnull=True, unique=True, comment="Tag used to add specific information regarding chemicals"),
                format = '%(name)s', singular="Property type", plural="Property types")

db.define_table('chemicals_chemical_property',
                Field('chemical', type='reference chemicals_chemical'),
                Field('tag', type='reference chemicals_tag_type'),
                Field('value_str', requires=IS_EMPTY_OR(IS_LENGTH(512)), comment="Property value if string"),
                Field('value_bool', type='boolean', comment="Property value if boolean"),
                Field('value_float', type='double', requires=IS_EMPTY_OR(IS_FLOAT_IN_RANGE(-1e100, 1e100)), comment="Property value if real number (1.2232)"),
                Field('value_int', type='integer', requires=IS_EMPTY_OR(IS_INT_IN_RANGE(-1e100, 1e100)), comment="Property value if integer"),
                singular="Property", plural="Properties" )

# manufacturers and resellers
db.define_table('chemicals_company',
                Field('name', required=True, notnull=True, unique=True),
                Field('url'),
                Field('email'),
                Field('comments'),
                format = '%(name)s', singular="Company", plural="Companies")

# locations
db.define_table('chemicals_location',
                Field('name', required=True, notnull=True, unique=True),
                Field('comments'),
                format = '%(name)s', singular="Location", plural="Locations")

# lots
db.define_table('chemicals_lot',
                Field('chemical', type='reference chemicals_chemical'),
                Field('manufacturer', type='reference chemicals_company'),
                Field('reseller', type='reference chemicals_company'),
                Field('location_name', type='reference chemicals_location'),
                Field('manufacturer_lot'),
                Field('available', type='boolean', required=True, notnull=True),
                Field('opened', type='date'),
                Field('expiry', type='date'),
                Field('comments'),
                format=lambda r: r.chemical.name + " / " + r.manufacturer.name + " / "  + r.manufacturer_lot,
                singular="Lot", plural="Lots")

db.define_table('chemicals_lot_property',
                Field('chemical', type='reference chemicals_lot'),
                Field('tag', type='reference chemicals_tag_type'),
                Field('value_str', requires=IS_EMPTY_OR(IS_LENGTH(512)), comment="Property value if string"),
                Field('value_bool', type='boolean', comment="Property value if boolean"),
                Field('value_float', type='double', requires=IS_EMPTY_OR(IS_FLOAT_IN_RANGE(-1e100, 1e100)), comment="Property value if real number (1.2232)"),
                Field('value_int', type='integer', requires=IS_EMPTY_OR(IS_INT_IN_RANGE(-1e100, 1e100)), comment="Property value if integer"),
                singular="Property", plural="Properties" )

# solutions
db.define_table('chemicals_solution',
                Field('name'),
                Field('composed_on', type='datetime', required=True, notnull=True, default=request.now),
                Field('available', type='boolean', default=True, required=True, notnull=True),
                Field('location_name', type='reference chemicals_location'),
                Field('comments'),
                format=lambda r: le('chemicals_solution', r.id, '%s / %s' % (r.name,str(r.id))),
                singular="Solution", plural="Solutions")

db.define_table('chemicals_solution_composition',
                Field('solution', type='reference chemicals_solution'),
                Field('component_solution', type='reference chemicals_solution',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, "chemicals_solution.id",
                                                    '%(name)s / %(composed_on)s / %(available)s / %(id)s'))),
                Field('component_chemical', type='reference chemicals_lot',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, "chemicals_lot.id",
                                                    lambda r: r.chemical.name + " / " + r.manufacturer.name + " / "  + r.manufacturer_lot
                      ))),
                Field('unit', type='reference unit', required=True, notnull=True),
                Field('amount', type='double', required=True, notnull=True),
                format=lambda r: r.chemical.name + "" + r.solution.name,
                singular="Component", plural="Components")

# virtual fields that help to display the data
db.chemicals_chemical.available_lots = Field.Virtual('available_lots',
                                                     lambda row: db( (db.chemicals_lot.chemical==row.chemicals_chemical.id) &
                                                                     (db.chemicals_lot.available==True) ).count())
