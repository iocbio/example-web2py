# link experiment IDs imported by kinetics to isolations

# externally created kinetics experiments table
db.define_table('kinetics_experiment',
                Field('experiment_id', type='text'),
                Field('type_generic', type='text'),
                Field('hardware', type='text'),
                Field('date', type='text'),
                primarykey=['experiment_id'],
                migrate=False)

# link kinetics measurements to mouse isolation
db.define_table('kinetics_experiment_to_mouse_cardiomyocytes',
                Field('preparation', type='reference prep_cardiomyocyte_mouse', notnull=True),
                Field('experiment', type='text', notnull=True, unique=True),
                format = lambda r: str(r.id) + " ExpLink / " + str(r.preparation.id) + " PrepID",
                singular="Kinetics experiment", plural="Kinetics experiments")

#db.kinetics_experiment_to_mouse_cardiomyocytes.experiment.requires = IS_IN_DB(db,'kinetics_experiment.experiment_id')

# view
db.define_table('kinetics_experiment_with_mouse',
                Field('experiment_id'),
                Field('exp_date', label='Date'),
                Field('exp_time', label='Time'),
                Field('type_generic', label='Type'),
                Field('hardware'),
                Field('cm_preparation', label='CM preparation', type='reference prep_cardiomyocyte_mouse'),
                Field('cm_ready', label='Cells ready', type="datetime"),
                Field('anim_id', label='Animal ID', type='reference animals_mouse'),
                Field('sex'),
                Field('spid', label='Phenotype'),
                Field('age', type='double'),
                migrate=False,
                singular="Kinetics experiment mouse CM", plural="Kinetics experiments mouse CM")
