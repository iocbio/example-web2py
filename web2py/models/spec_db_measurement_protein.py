from datetime import datetime

def sample_or_buffer(buffer):
    if buffer: return 'Buffer'
    return 'Sample'

db.define_table('measurement_protein_batch',
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('buffer', type='boolean', default=False, label='Buffer offset'),
                Field('comments'),
                format=lambda r: le('measurement_protein_batch', r.id, str(r.id) + ' / ' + str(r.mtime) + " / " + sample_or_buffer(r.buffer)),
                singular="Protein measurements batch", plural="Protein measurements batches")

db.define_table('measurement_protein_spectrum_280_330nm',
                Field('batch', type='reference measurement_protein_batch', comment="Protein measurement batch", notnull=True),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('dilution', type="double", default=10.0, comment=dilution_comment),
                Field('absorbance_280', type="double", notnull=True, comment="Absorbance at 280nm"),
                Field('absorbance_330', type="double", notnull=True, comment="Absorbance at 330nm"),
                singular="Protein absorbance spectrum", plural="Protein absorbance spectra")

# view
db.define_table('measurement_protein_content_280_330nm',
                Field('id', type='reference measurement_protein_spectrum_280_330nm', label='Spectrum ID'),
                Field('batch', type='reference measurement_protein_batch', comment="Protein measurement batch"),
                Field('success', type='boolean', label='Use this measurement in statistics'),
                Field('dilution', type="double", comment=dilution_comment),
                Field('absorbance_280', type="double", comment="Absorbance at 280nm"),
                Field('absorbance_330', type="double", comment="Absorbance at 330nm"),
                Field('protein', type="double", comment="Undiluted protein content [ug/ul]"),
                migrate=False)

# view
db.define_table('measurement_protein_batch_content',
                Field('id', type='reference measurement_protein_batch', comment="Protein measurement batch"),
                Field('mtime', type="datetime", label="Measurement time", comment="Time at which measurements were performed"),
                Field('buffer', type='boolean', label='Buffer offset'),
                Field('protein', type="double", comment="Undiluted protein content [ug/ul]"),
                Field('protein_std', type="double", label='STD', comment="STD of protein content [ug/ul]"),
                Field('n', type="integer", comment="Number of measurements"),
                Field('comments'),
                migrate=False)
