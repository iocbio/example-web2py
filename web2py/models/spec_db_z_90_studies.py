# List of studies

db.define_table('study_respiration_aging_gamt_lucia',
                Field('experiment_id', type='text'),
                Field('success', type='boolean', notnull=True, comment='Success'),
                Field('comment', type='text', label='Comment'),
                singular="Study of respiration changes due to aging on GAMT KO/WT (Lucia)",
                plural="Study of respiration changes due to aging on GAMT KO/WT (Lucia)"
                )

db.define_table('study_sparks_demo_iso',
                Field('experiment_id', type='text'),
                Field('iso', type='boolean', notnull=True, comment='In presence of ISO'),
                Field('order_in_batch', type='integer', notnull=True, default=1, comment='Which cell was it in the batch'),
                Field('success', type='boolean', default=True, notnull=True, comment='Success'),
                Field('comment', type='text', label='Comment'),
                singular='Study for sparks software paper showing ISO effect',
                plural='Study for sparks software paper showing ISO effect')

db.define_table('study_fractionation_hkakck_mouse',
                Field('mouse', type='reference prep_animal_mouse', comment="Mouse preparation", notnull=True),
                Field('success', type='boolean', default=True, notnull=True, comment='Success'),
                Field('comment', type='text', label='Comment'),
                singular='Fractination study of HK/AK/CK',
                plural='Fractination study of HK/AK/CK')

db.define_table('study_agat_gamt_skeletal',
                Field('mouse', type='reference prep_animal_mouse', comment="Mouse preparation", notnull=True),
                Field('success', type='boolean', default=True, notnull=True, comment='Success'),
                Field('comment', type='text', label='Comment'),
                singular='AGAT/GAMT skeletal muscle',
                plural='AGAT/GAMT skeletal muscle')

db.define_table('study_respiration_hkakck_mouse',
                Field('experiment_id', type='text'),
                Field('success', type='boolean', default=True, notnull=True, comment='Success'),
                Field('comment', type='text', label='Comment'),
                singular='Respiration study of HK/AK/CK',
                plural='Respiration study of HK/AK/CK')

db.define_table('study_compartmentation_agat_cm_mouse',
                Field('experiment_id', type='text'),
                Field('success', type='boolean', default=True, notnull=True, comment='Success'),
                Field('comment', type='text', label='Comment'),
                singular='Study of intracellular compartmentation in AGAT CM',
                plural='Study of intracellular compartmentation in AGAT CM')

db.define_table('study_respiration_c57malefemale_mouse',
                Field('experiment_id', type='text'),
                Field('success', type='boolean', default=True, notnull=True, comment='Success'),
                Field('comment', type='text', label='Comment'),
                singular='Respiration study of C57 male and female',
                plural='Respiration study of C57 male and female')

db.define_table('study_maanus_msc',
                Field('experiment_id', type='text'),
                Field('iso', type='boolean', notnull=True, comment='In presence of ISO'),
                Field('success_pacing', type='boolean', default=True, notnull=True, comment='Success: pacing phase'),
                Field('success_sparks', type='boolean', default=True, notnull=True, comment='Success: sparks measurements'),
                Field('comment', type='text', label='Comment'),
                singular='Maanus MSc on Ca sparks and transients',
                plural='Maanus MSc on Ca sparks and transients')

db.define_table('study_sparks_agat',
                Field('experiment_id', type='text'),
                Field('iso', type='boolean', notnull=True, comment='In presence of ISO'),
                Field('success_pacing', type='boolean', default=True, notnull=True, comment='Success: pacing phase'),
                Field('success_sparks', type='boolean', default=True, notnull=True, comment='Success: sparks measurements'),
                Field('comment', type='text', label='Comment'),
                singular='AGAT Ca sparks and transients',
                plural='AGAT Ca sparks and transients')

db.define_table('study_sparks_gamt',
                Field('experiment_id', type='text'),
                Field('iso', type='boolean', notnull=True, comment='In presence of ISO'),
                Field('success_pacing', type='boolean', default=True, notnull=True, comment='Success: pacing phase'),
                Field('success_sparks', type='boolean', default=True, notnull=True, comment='Success: sparks measurements'),
                Field('comment', type='text', label='Comment'),
                singular='GAMT Ca sparks and transients',
                plural='GAMT Ca sparks and transients')

