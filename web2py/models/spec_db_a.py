# This must be executed after db.py. For that, it should be naimed so
# that alphabetical order is after db.py. This file defines tables that
# are of general interest and can be used everywhere

db.define_table('unit',
                Field('name', required=True, notnull=True, unique=True),
                format = '%(name)s')

db.define_table('muscle_type',
                Field('name', required=True, notnull=True, unique=True),
                format = '%(name)s')

db.define_table('fraction_type',
                Field('name', required=True, notnull=True, unique=True),
                format = '%(name)s')

def le(table, id, txt):
    return A(str(txt), _href=URL(c="default", f="display",
                            args=[table, id]))

dilution_comment = 'How many times was the sample diluted. For example, when using 1 part of solution and one part of sample, dilution would be 2.0. Dilution 1.0 corresponds to no dilution'
