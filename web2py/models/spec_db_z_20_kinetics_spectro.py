# link experiment IDs imported by kinetics to isolations

# externally created kinetics spectro table
db.define_table('kinetics_spectro',
                Field('experiment_id', type='text'),
                primarykey=['experiment_id'],
                migrate=False)

# link kinetics spectro to experiment data
db.define_table('kinetics_spectro_config',
                Field('experiment', type='text', notnull=True, unique=True),
                Field('cells_in_mkl', type="double", label='Cells [microliters]', comment="Amount of cells in the chamber [microliters]"),
                Field('chamber_volume_in_ml', type="double", default=2.0, label='Chamber volume [ml]', comment="Chamber volume [milliliters]"),
                Field('temperature', type='double', default=25.0, label='Temperature [C]'),
                singular="Spectro experiment data", plural="Spectro experiment data")
