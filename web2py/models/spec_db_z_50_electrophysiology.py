# This must be executed after db.py and spec_db_prep. For that, it should
# be named so that alphabetical order is after those files

db.define_table('prep_electrophysiology_cardiomyocyte',
                Field('preparation', type='reference prep_cardiomyocyte_mouse', notnull=True),
                Field('comment', type='text', label='Comment'),
                Field('pipette_solution', type='reference chemicals_solution', notnull=True),
                Field('r_pip', type='double', label='Pipette resistance [MOhm]'),
                Field('c_pip', type='double', label='Pipette capacitance [pF]'),
                Field('v_j', type='double', label='Junction potential [mV]'),
                Field('r_ser', type='double', label='Series resistance [MOhm]'),
                Field('c_mem', type='double', label='Membrane capacitance [pF]'),
                format = lambda r: str(r.id) + " / " + str(r.preparation.id) + " " + str(r.preparation.ready) + " / " + str(r.preparation.isolation.preparation_animal.animal.id),
                singular="Cell used in electrophysiology", plural="Cells used in electrophysiology")

db.define_table('measurement_electrophysiology_cafluxes_iv',
                Field('cell', type='reference prep_electrophysiology_cardiomyocyte',
                      comment='Cell ID / Cardiomyocyte preparation ID / Animal ID',
                      notnull=True, unique=True),
                Field('success', type='boolean', notnull=True, comment='Overall success'),
                Field('ltcc_ttx', type='boolean', notnull=True, comment='LTCC with tetrodotoxin'),
                Field('ltcc_iso', type='boolean', notnull=True, comment='LTCC with isoprenaline'),
                Field('srload_ttx', type='boolean', notnull=True, comment='SR load with tetrodotoxin'),
                Field('srload_iso', type='boolean', notnull=True, comment='SR load with isoprenaline'),
                Field('srrecovery_ttx', type='boolean', comment='SR recovery after caffeine with tetrodotoxin'),
                Field('srrecovery_iso', type='boolean', comment='SR recovery after caffeine with isoprenaline'),
                Field('kill', type='boolean', notnull=True, comment='Maximal fluorescence estimated at high calcium measurement'),
                Field('comment', type='text', label='Comment'),
                format = lambda r: str(r.id),
                singular="Ca fluxes via IV", plural="Ca fluxes via IV")

# read only: used only to create virtual field
db.define_table('kinetics_electrophysiology',
                Field('experiment_id'),
                Field('cell', type='integer'),
                Field('filename'),
                Field('mode'),
                Field('title'),
                Field('tag'),
                primarykey=['experiment_id'],
                migrate=False)

def ke_title(cell_id):
    title = ''
    for row in db(db.kinetics_electrophysiology.cell == cell_id).select(orderby=db.kinetics_electrophysiology.title):
        title += row.title + ", "
    if len(title) > 2: title = title[:-2]
    return title

db.prep_electrophysiology_cardiomyocyte.titles = Field.Virtual('titles',
                                                               lambda row: ke_title(row.prep_electrophysiology_cardiomyocyte.id))
