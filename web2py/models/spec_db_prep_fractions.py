from datetime import datetime

db.define_table('prep_mouse_muscle',
                Field('mouse', type='reference prep_animal_mouse', comment="Mouse preparation", notnull=True),
                Field('muscle', type='reference muscle_type', comment="Muscle type", notnull=True),
                Field('weight', type="double", comment="Weight [mg]"),
                Field('homogenate', type="double", comment="Concentration of whole tissue homogenate [mg/ml]"),
                format=lambda r: le('prep_mouse_muscle', r.id, str(r.mouse.animal.id) + " mouse / " + str(r.muscle.name) + ' / ' + str(r.id) + " muscle"),
                singular="Mouse muscle", plural="Mouse muscles")


db.define_table('measurement_mouse_muscle_protein',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", default=1, notnull=True),
                Field('sample', type='reference measurement_protein_batch', comment='Protein measurements batch with the muscle sample', notnull=True),
                Field('buffer', type='reference measurement_protein_batch', comment='Protein measurements batch with plain buffer', notnull=True),
                Field('comments'))

db.measurement_mouse_muscle_protein.sample.requires = IS_IN_DB(db(db.measurement_protein_batch.buffer != True),
                                                               'measurement_protein_batch.id',
                                                               '%(id)s / %(mtime)s')
db.measurement_mouse_muscle_protein.buffer.requires = IS_IN_DB(db(db.measurement_protein_batch.buffer == True),
                                                               'measurement_protein_batch.id',
                                                               '%(id)s / %(mtime)s')

# view
db.define_table('measurement_mouse_muscle_protein_value',
                Field('id', type='reference measurement_mouse_muscle_protein', comment='Muscle protein measurement'),
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation"),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle"),
                Field('sample', type='reference measurement_protein_batch', comment='Protein measurements batch with the muscle sample'),
                Field('buffer', type='reference measurement_protein_batch', comment='Protein measurements batch with plain buffer'),
                Field('protein', type='double', comment="Protein content [ug/ul]"),
                Field('comments'),
                migrate=False,
                singular="Protein", plural="Proteins")

###############################################################
# enzymes

# CS

db.define_table('measurement_mouse_muscle_citrate_synthase',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", default=1, notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('volume_cuvette', type="double", notnull=True, default=1.0, comment="Cuvette volume [ml]"),
                Field('volume_sample', type="double", notnull=True, default=5.0, comment="Sample volume [microliter]"),
                Field('dilution', type="double", default=10.0, comment=dilution_comment),
                Field('sample', type="double", notnull=True, comment='Slope after addition of sample [Abs/min]'),
                Field('oxaloacetate', type="double", notnull=True, comment='Slope after addition of oxaloacetate [Abs/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                singular="CS", plural="CS")

db.define_table('measurement_mouse_muscle_citrate_synthase_rate',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('rate_conc_umol_perul_permin', type="double", comment='Rate for absolute change per added sample volume [umol/ul/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                migrate=False,
                singular="CS rate", plural="CS rate")

db.define_table('measurement_mouse_muscle_citrate_synthase_rate_protein',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('rate', type="double", comment='Rate [umol/min/g prot]'),
                Field('std', type="double", comment='STD for rate [umol/min/g prot]'),
                Field('n', type='integer'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                migrate=False,
                singular="CS rate protein", plural="CS rate protein")

db.define_table('measurement_mouse_muscle_citrate_synthase_rate_ww',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('rate', type="double", comment='Rate [umol/min/g ww]'),
                Field('std', type="double", comment='STD for rate [umol/min/g ww]'),
                Field('n', type='integer'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                migrate=False,
                singular="CS rate ww", plural="CS rate protein")

# AK and CK

db.define_table('measurement_mouse_muscle_ak_ck',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", default=1, notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('volume_cuvette', type="double", notnull=True, default=1.0, comment="Cuvette volume [ml]"),
                Field('volume_sample', type="double", notnull=True, default=5.0, comment="Sample volume [microliter]"),
                Field('dilution', type="double", default=10.0, comment=dilution_comment),
                Field('ak', type="double", notnull=True, comment='Slope in the presence of adenylate kinase [Abs/min]'),
                Field('ak_ck', type="double", notnull=True, comment='Slope in the presence of adenylate kinase and creatine kinase [Abs/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                singular="AK+CK", plural="AK+CK")

db.define_table('measurement_mouse_muscle_ak_ck_rate',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('ak_umol_perul_permin', type="double", comment='AK rate for absolute change per added sample volume [umol/ul/min]'),
                Field('ak_ck_umol_perul_permin', type="double", comment='Combined AK and CK rates for absolute change per added sample volume [umol/ul/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                migrate=False,
                singular="AK+CK rate", plural="AK+CK rate")

db.define_table('measurement_mouse_muscle_ak_ck_rate_protein',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('ak', type="double", comment='Adenylate Kinase rate [umol/min/g prot]'),
                Field('ak_std', type="double", comment='STD for Adenylate Kinase rate [umol/min/g prot]'),
                Field('ak_ck', type="double", comment='Combined Adenylate Kinase and Creatine Kinase rates [umol/min/g prot]'),
                Field('ak_ck_std', type="double", comment='STD for combined Adenylate Kinase and Creatine Kinase rates [umol/min/g prot]'),
                Field('ck', type="double", comment='Creatine Kinase rate [umol/min/g prot]'),
                Field('ck_std', type="double", comment='STD for Creatine Kinase rate [umol/min/g prot]'),
                Field('n', type='integer'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                migrate=False,
                singular="AK+CK rate protein", plural="AK+CK rate protein")

db.define_table('measurement_mouse_muscle_ak_ck_rate_ww',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('ak', type="double", comment='Adenylate Kinase rate [umol/min/g ww]'),
                Field('ak_std', type="double", comment='STD for Adenylate Kinase rate [umol/min/g ww]'),
                Field('ak_ck', type="double", comment='Combined Adenylate Kinase and Creatine Kinase rates [umol/min/g ww]'),
                Field('ak_ck_std', type="double", comment='STD for combined Adenylate Kinase and Creatine Kinase rates [umol/min/g ww]'),
                Field('ck', type="double", comment='Creatine Kinase rate [umol/min/g ww]'),
                Field('ck_std', type="double", comment='STD for Creatine Kinase rate [umol/min/g ww]'),
                Field('n', type='integer'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                migrate=False,
                singular="AK+CK rate ww", plural="AK+CK rate ww")

# HK

db.define_table('measurement_mouse_muscle_hexokinase',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", default=1, notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('volume_cuvette', type="double", notnull=True, default=1.0, comment="Cuvette volume [ml]"),
                Field('volume_sample', type="double", notnull=True, default=5.0, comment="Sample volume [microliter]"),
                Field('dilution', type="double", default=10.0, comment=dilution_comment),
                Field('rate', type="double", notnull=True, comment='Slope in the presence of hexokinase [Abs/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                singular="HK", plural="HK")

db.define_table('measurement_mouse_muscle_hexokinase_rate',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('rate_conc_umol_perul_permin', type="double", comment='Rate for absolute change per added sample volume [umol/ul/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                migrate=False,
                singular="HK rate", plural="HK rate")

db.define_table('measurement_mouse_muscle_hexokinase_rate_protein',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('rate', type="double", comment='Rate [umol/min/g prot]'),
                Field('std', type="double", comment='STD for rate [umol/min/g prot]'),
                Field('n', type='integer'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                migrate=False,
                singular="HK rate protein", plural="HK rate protein")

# LDH

db.define_table('measurement_mouse_muscle_ldh',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", default=1, notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('volume_cuvette', type="double", notnull=True, default=1.0, comment="Cuvette volume [ml]"),
                Field('volume_sample', type="double", notnull=True, default=5.0, comment="Sample volume [microliter]"),
                Field('dilution', type="double", default=10.0, comment=dilution_comment),
                Field('baseline', type="double", notnull=True, comment='Baseline slope [Abs/min]'),
                Field('pyruvate_low', type="double", notnull=True, comment='Slope in the presence of low pyruvate [Abs/min]'),
                Field('pyruvate_high', type="double",
                      requires=IS_EMPTY_OR(IS_FLOAT_IN_RANGE(-1e100, 1e100)),
                      comment='Slope in the presence of high pyruvate [Abs/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                singular="LDH", plural="LDH")

db.define_table('measurement_mouse_muscle_ldh_rate',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('pyruvate_low_umol_perul_permin', type="double", comment='Rate in the presence of low pyruvate for absolute change per added sample volume [umol/ul/min]'),
                Field('pyruvate_high_umol_perul_permin', type="double", comment='Rate in the presence of low pyruvate for absolute change per added sample volume [umol/ul/min]'),
                Field('high2low_ratio', type="double", comment='Ratio of rates in high-to-low pyruvate concentrations [no units]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                migrate=False,
                singular="LDH rate", plural="LDH rate")

db.define_table('measurement_mouse_muscle_ldh_rate_protein',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('pyruvate_low', type="double", comment='Rate in the presence of low pyruvate [umol/min/g prot]'),
                Field('pyruvate_low_std', type="double", comment='STD for rate in the presence of low pyruvate [umol/min/g prot]'),
                Field('pyruvate_high', type="double", comment='Rate in the presence of high pyruvate [umol/min/g prot]'),
                Field('pyruvate_high_std', type="double", comment='STD for rate in the presence of high pyruvate [umol/min/g prot]'),
                Field('high2low_ratio', type="double", comment='Ratio of rates in high-to-low pyruvate concentrations [no units]'),
                Field('high2low_ratio_std', type="double", comment='STD for ratio of rates in high-to-low pyruvate concentrations [no units]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('n', type='integer'),
                migrate=False,
                singular="LDH rate protein", plural="LDH rate protein")

db.define_table('measurement_mouse_muscle_ldh_rate_ww',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('pyruvate_low', type="double", comment='Rate in the presence of low pyruvate [umol/min/g ww]'),
                Field('pyruvate_low_std', type="double", comment='STD for rate in the presence of low pyruvate [umol/min/g ww]'),
                Field('pyruvate_high', type="double", comment='Rate in the presence of high pyruvate [umol/min/g ww]'),
                Field('pyruvate_high_std', type="double", comment='STD for rate in the presence of high pyruvate [umol/min/g ww]'),
                Field('high2low_ratio', type="double", comment='Ratio of rates in high-to-low pyruvate concentrations [no units]'),
                Field('high2low_ratio_std', type="double", comment='STD for ratio of rates in high-to-low pyruvate concentrations [no units]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('n', type='integer'),
                migrate=False,
                singular="LDH rate ww", plural="LDH rate ww")

# PK

db.define_table('measurement_mouse_muscle_pyruvate_kinase',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", default=1, notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('volume_cuvette', type="double", notnull=True, default=1.0, comment="Cuvette volume [ml]"),
                Field('volume_sample', type="double", notnull=True, default=5.0, comment="Sample volume [microliter]"),
                Field('dilution', type="double", default=10.0, comment=dilution_comment),
                Field('baseline', type="double", default=0.002, notnull=True, comment='Baseline slope [Abs/min]'),
                Field('rate', type="double", notnull=True, comment='Slope in the presence of pyruvate kinase [Abs/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                singular="PK", plural="PK")

db.define_table('measurement_mouse_muscle_pyruvate_kinase_rate',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('rate_conc_umol_perul_permin', type="double", comment='Rate for absolute change per added sample volume [umol/ul/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                migrate=False,
                singular="PK rate", plural="PK rate")

db.define_table('measurement_mouse_muscle_pyruvate_kinase_rate_protein',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('rate', type="double", comment='Rate [umol/min/g prot]'),
                Field('std', type="double", comment='STD for rate [umol/min/g prot]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('n', type='integer'),
                migrate=False,
                singular="PK rate protein", plural="PK rate protein")

db.define_table('measurement_mouse_muscle_pyruvate_kinase_rate_ww',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('rate', type="double", comment='Rate [umol/min/g ww]'),
                Field('std', type="double", comment='STD for rate [umol/min/g ww]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('n', type='integer'),
                migrate=False,
                singular="PK rate ww", plural="PK rate ww")

# Cytochrome Oxidase
db.define_table('measurement_mouse_muscle_cytochrome_oxidase',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", default=1, notnull=True),
                Field('experiment', type='text', notnull=True, unique=True),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('volume_sample', type="double", notnull=True, default=5.0, comment="Sample volume [microliter]"),
                Field('dilution', type="double", default=1.0, comment=dilution_comment),
                Field('temperature', type="double", notnull=True, default=37.0, comment="Temperature in C"),
                Field('comments'),
                singular="CytOx", plural="CytOx")

db.define_table('measurement_mouse_muscle_cytochrome_oxidase_rate_constant_ww',
                Field('muscle', type='reference prep_mouse_muscle', comment="Mouse muscle preparation", notnull=True),
                Field('fraction', type='reference fraction_type', comment="Fraction of the muscle", notnull=True),
                Field('rate_constant', type="double", comment='Rate constant [1/min/g ww]'),
                Field('std', type="double", comment='STD for rate constant [1/min/g ww]'),
                Field('n', type='integer'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                migrate=False,
                singular="CytOx rate ww", plural="CytOx rate ww")

