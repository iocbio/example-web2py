# should be after spec_db_prep_animals

db.define_table('prep_cardiomyocyte_mouse',
                Field('isolation', type='reference prep_proc_isolation_cardio_mouse', notnull=True),
                Field("ready", type="datetime", notnull=True, default=datetime.now, label="Preparation prepared", comment="Time at which preparation was ready"),
                Field("time_moment", type="double",  notnull=True, comment="Time moment at which the cells were taken [min]"),
                Field('protein', type="double", comment="Protein content [mg prot/cell volume ml]"),
                Field('yield_volume', type="double", label="Yield volume", comment="How many cells isolated (sedimented only) [ml]"),
                Field('rod', type="double", comment="Concentration of the cells (rod shaped) [cells/ml]"),
                Field('dead', type="double", comment="Concentration of the cells (dead) [cells/ml]"),
                Field('unknown', type="double", comment="Concentration of the cells (not classified under dead or rod) [cells/ml]"),
                Field('comments'),
                format=lambda r: le('prep_cardiomyocyte_mouse', r.id, str(r.id) + ' CM / ' + str(r.isolation.preparation_animal.animal.id) + " AnimID / " + str(r.ready)),
                singular="Cardiomyocytes", plural="Cardiomyocytes")

db.define_table('measurement_mouse_cardiomyocyte_protein',
                Field('cardiomyocytes', type='reference prep_cardiomyocyte_mouse', notnull=True, comment="Cardiomyocytes preparation"),
                Field('sample', type='reference measurement_protein_batch', comment='Protein measurements batch with the cardiomyocytes sample', notnull=True),
                Field('buffer', type='reference measurement_protein_batch', comment='Protein measurements batch with plain buffer', notnull=True),
                Field('comments'))

db.measurement_mouse_cardiomyocyte_protein.sample.requires = IS_IN_DB(db(db.measurement_protein_batch.buffer != True),
                                                                      'measurement_protein_batch.id',
                                                                      '%(id)s / %(mtime)s')
db.measurement_mouse_cardiomyocyte_protein.buffer.requires = IS_IN_DB(db(db.measurement_protein_batch.buffer == True),
                                                                      'measurement_protein_batch.id',
                                                                      '%(id)s / %(mtime)s')

# view
db.define_table('measurement_mouse_cardiomyocyte_protein_value',
                Field('id', type='reference measurement_cardiomyocyte_protein', comment='Cardiomyocytes protein measurement'),
                Field('cardiomyocytes', type='reference prep_cardiomyocyte_mouse', notnull=True, comment="Cardiomyocytes preparation"),
                Field('sample', type='reference measurement_protein_batch', comment='Protein measurements batch with the muscle sample'),
                Field('buffer', type='reference measurement_protein_batch', comment='Protein measurements batch with plain buffer'),
                Field('protein', type='double', comment="Protein content [ug/ul]"),
                Field('comments'),
                migrate=False,
                singular="Protein", plural="Proteins")

# enzymes
db.define_table('measurement_mouse_cardiomyocyte_citrate_synthase',
                Field('cardiomyocytes', type='reference prep_cardiomyocyte_mouse', notnull=True, comment="Cardiomyocytes preparation"),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('volume_cuvette', type="double", notnull=True, default=1.0, comment="Cuvette volume [ml]"),
                Field('volume_sample', type="double", notnull=True, default=5.0, comment="Sample volume [microliter]"),
                Field('dilution', type="double", default=10.0, comment=dilution_comment),
                Field('sample', type="double", notnull=True, comment='Slope after addition of sample [Abs/min]'),
                Field('oxaloacetate', type="double", notnull=True, comment='Slope after addition of oxaloacetate [Abs/min]'),
                Field('temperature', type="double", notnull=True, default=25.0, comment="Temperature in C"),
                Field('comments'),
                singular="CS", plural="CS")

db.define_table('measurement_mouse_cardiomyocyte_citrate_synthase_rate',
                Field('cardiomyocytes', type='reference prep_cardiomyocyte_mouse', notnull=True, comment="Cardiomyocytes preparation"),
                Field('mtime', type="datetime", notnull=True, default=datetime.now, label="Measurement time", comment="Time at which measurements were performed"),
                Field('success', type='boolean', default=True, comment='Use this measurement in statistics'),
                Field('rate_conc_umol_perul_permin', type="double", comment='Rate for absolute change per added sample volume [umol/ul/min]'),
                Field('temperature', type="double", default=25.0, comment="Temperature in C"),
                Field('comments'),
                migrate=False,
                singular="CS rate", plural="CS rate")

db.define_table('measurement_mouse_cardiomyocyte_citrate_synthase_rate_protein',
                Field('cardiomyocytes', type='reference prep_cardiomyocyte_mouse', notnull=True, comment="Cardiomyocytes preparation"),
                Field('rate', type="double", comment='Rate [umol/min/g protein]'),
                Field('std', type="double", comment='STD for rate [umol/min/g protein]'),
                Field('n', type='integer'),
                Field('temperature', type="double", default=25.0, comment="Temperature in C"),
                migrate=False,
                singular="CS rate protein", plural="CS rate protein")
