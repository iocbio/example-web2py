import psycopg2
import psycopg2.extras
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

def process(v):
    c = []
    for i in v:
        if i is not None:
            try:
                c.append(unicode(i.strip()))
            except:
                c.append(i)
        else: c.append(i)
    print c
    return tuple(c)

# csqlite = sqlite3.connect('databases/storage.sqlite')
# cursor_sqlite = csqlite.cursor()

conn = psycopg2.connect("dbname=experiments")
connTo = psycopg2.connect("dbname=experiments_v2")

cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
cursor_target = connTo.cursor(cursor_factory=psycopg2.extras.DictCursor)

# # chemicals.chemical
# cur.execute("SELECT name, formula FROM chemicals.chemical;")
# while True:
#     v = cur.fetchone()
#     if v is not None:
#         c = []
#         for i in v: c.append(unicode(i.strip()))
#         print c
#         cursor_target.execute("SELECT COUNT(*) FROM chemicals_chemical WHERE name=%s", [c[0]])
#         if cursor_target.fetchone()[0] == 0:
#             cursor_target.execute("INSERT INTO chemicals_chemical (name,formula) VALUES (%s,%s)",
#                                   tuple(c))            
#     else: break

# ###############################################
# # chemicals location
# cur.execute("SELECT name, description FROM chemicals.location;")
# while True:
#     v = cur.fetchone()
#     if v is not None:
#         c = process(v)
#         cursor_target.execute("INSERT INTO chemicals_location (name,comments) VALUES (%s,%s)",
#                               tuple(c))
#     else: break

# ###############################################
# # chemicals manufacturers
# cur.execute("SELECT name FROM chemicals.manufacturer;")
# while True:
#     v = cur.fetchone()
#     if v is not None:
#         c = process(v)
#         cursor_target.execute("INSERT INTO chemicals_company (name) VALUES (%s)", c)
#     else: break

# ###############################################
# # chemicals resellers
# cur.execute("SELECT name,contact,comment FROM chemicals.seller;")
# while True:
#     v = cur.fetchone()
#     if v is not None:
#         c = process(v)
#         cursor_target.execute("SELECT COUNT(*) FROM chemicals_company WHERE name=%s", [c[0]])
#         if cursor_target.fetchone()[0] == 0:
#             cursor_target.execute("INSERT INTO chemicals_company (name,email,comments) VALUES (%s,%s,%s)", c)
#         else:
#             cursor_target.execute("UPDATE chemicals_company SET email=%s, comments=%s WHERE name=%s", [ c[1], c[2], c[0] ] )
#     else: break

# ###############################################
# # prep species
# cur.execute('SELECT spid,full_type,origin,"from",till,comment FROM prep.species;')
# while True:
#     v = cur.fetchone()
#     if v is not None:
#         c = process(v)
#         cursor_target.execute("INSERT INTO prep_species (spid, sptype, origin, available_from, available_till,comments) VALUES (%s,%s,%s,%s,%s,%s)", c)
#     else: break


# ###############################################
# # animals mice

# get species IDs first
species = {}
cursor_target.execute("SELECT id,spid FROM prep_species")
for c in cursor_target.fetchall():
    species[c[1]] = c[0]

animals_old = {}
breeding_old = {}

cur.execute('SELECT mouse_id,earmark,sex,spid,breeding_id,generation_no,cage_no,death,comment FROM animals.mice')
while True:
    v = cur.fetchone()
    if v is not None:
        v = dict(v)
        animals_old[ v['mouse_id'] ] = v
    else: break
        
cur.execute('SELECT breeding_id,female_id,male_id,birth,success,comment FROM animals.mice_breeding')
while True:
    v = cur.fetchone()
    if v is not None:
        v = dict(v)
        breeding_old[ v['breeding_id'] ] = v
    else: break

breeding_pairs_old = {}
def bpk(v):
    return "Male: " + str(v['male_id']) + " - Female: " + str(v['female_id'])

def lk(p, d):
    return p + " " + str(d)

for k in breeding_old:
    v = breeding_old[k]
    breeding_pairs_old[ bpk(v) ] = v

# for k in breeding_pairs_old:
#     cursor_target.execute("INSERT INTO animals_mouse_breeding_pair (comments) VALUES(%s)", (k,))

breeding_pairs = {}
cursor_target.execute("SELECT * FROM animals_mouse_breeding_pair")
while True:
    v = cursor_target.fetchone()
    if v is not None:
        v = dict(v)
        breeding_pairs[ v["comments"] ] = v["id"]
    else: break

# for k in breeding_old:
#     v = breeding_old[k]
#     if v['success']: succ = 'T'
#     else: succ = 'F'
#     cursor_target.execute("INSERT INTO animals_mouse_litter (pair, birth, success, comments) VALUES(%s,%s,%s,%s)",
#                           ( breeding_pairs[bpk(v)], v['birth'], succ, v['comment'] ))

litter = {}
cursor_target.execute("SELECT litter.id as lid, birth, pair.comments FROM animals_mouse_litter litter JOIN animals_mouse_breeding_pair pair ON (pair.id=litter.pair)")
while True:
    v = cursor_target.fetchone()
    if v is not None:
        v = dict(v)
        litter[ lk(v['comments'],v['birth']) ] = v["lid"]
    else: break

# for k in animals_old:
#     ao = animals_old[k]
#     sex_old = ao["sex"]
#     if sex_old == "F": sex="Female"
#     elif sex_old == "M": sex="Male"
#     else:
#         print "Ukknown sex", sex_old
#         exit()
#     if ao['spid'] is not None: ao['spid'] = species[ao['spid']]

#     if ao['breeding_id'] is not None:
#         bo = breeding_old[ ao['breeding_id'] ]
#         lid = litter[ lk(bpk(bo), bo['birth']) ]
#     else:
#         lid = None

#     cursor_target.execute("INSERT INTO animals_mouse (id, earmark, sex, species, litter, cage, death, comments) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
#                           (ao['mouse_id'], ao['earmark'], sex, ao['spid'], lid, ao['cage_no'], ao['death'], ao['comment']))

# for k in breeding_pairs_old:
#     bo = breeding_pairs_old[k]
#     pid = breeding_pairs[ bpk(bo) ]
#     for parent in [ bo["male_id"], bo["female_id"] ]:
#         cursor_target.execute("INSERT INTO animals_mouse_breeding_pair_parents (pair, parent) VALUES (%s,%s)",
#                               (pid,parent) )
                          


connTo.commit()


