from records.records import Database
import keyring, datetime, json

username = "markov"
password = keyring.get_password("iocbio-kinetics", username)

dbold = Database("postgresql://" + username + ":" + password +
                 "@sysbio-db.kybi/experiments")

dbnew = Database("postgresql://" + username + ":" + password +
                 "@sysbio-db.kybi/experiments_v2")

Old2New = {}
for row in dbold.query("SELECT p.prepid, i.animid, i.spid, i.sex, i.date_of_birth FROM prep.preparation p " +
                       "JOIN prep.isolation_mouse_cardiomyocytes i ON p.isol_mouse_cardio=i.animid WHERE p.isol_mouse_cardio IS NOT NULL"):
    print(row)

    c = 0
    sex = "No idea"
    if row.sex == "M": sex="Male"
    elif row.sex == "F": sex="Female"

    animal = None
    death = row.animid.date()
    for rn in dbnew.query("SELECT id FROM animals_mouse_age WHERE sex=:sex AND spid=:spid AND birth=:birth AND death=:death",
                          sex=sex, spid=row.spid, birth=row.date_of_birth, death=death):
        animal=rn.id
        c+=1
    if c != 1:
        if c == 0:
            print("Oh no, cannot find it!")
        elif c != 1:
            print("Oh no, more than one hit!")
        continue

    oldprepid = str(row.prepid)
    print("Let's see if we have it in the new dbnew", oldprepid)

    if dbnew.query("SELECT COUNT(*) AS count FROM prep_animal_mouse WHERE animal=:id",
                    id=animal).first().count == 0:
        print('Preparation animal missing, creating')
        dbnew.query("INSERT INTO prep_animal_mouse(animal,ready,comments) VALUES(:id,:ready,:comments)",
                       id=animal, ready=row.animid,
                       comments="Automatically generated preparation data, isolation from old dbnew; old prepid=%s" % oldprepid)
    animrow = dbnew.query("SELECT id FROM prep_animal_mouse WHERE animal=:id", id=animal).first()
    prep_animal_id = animrow.id

    if dbnew.query("SELECT COUNT(*) AS count FROM prep_proc_isolation_cardio_mouse WHERE preparation_animal=:id",
                      id=prep_animal_id).first().count == 0:
        print('Cardiomyocytes isolation procedure missing, creating')
        dbnew.query("INSERT INTO prep_proc_isolation_cardio_mouse(started,preparation_animal,comments) VALUES(:date,:pid,:comments)",
                       date=datetime.datetime.combine(death, datetime.time(hour=10, minute=0)),
                       pid=prep_animal_id, comments="Automatically generated isolation procedure data from old database; old prepid=%s" % oldprepid)
    isol_id = dbnew.query("SELECT id FROM prep_proc_isolation_cardio_mouse WHERE preparation_animal=:id",
                             id=prep_animal_id).first().id

    if dbnew.query("SELECT COUNT(*) AS count FROM prep_cardiomyocyte_mouse WHERE isolation=:iid",
                      iid=isol_id).first().count == 0:
        print("Cardiomyocytes PREP ID is missing, creating")
        dbnew.query("INSERT INTO prep_cardiomyocyte_mouse(isolation,ready,comments) VALUES(:iid,:date,:comments)",
                       iid=isol_id, date=datetime.datetime.combine(death, datetime.time(hour=12, minute=0)),
                       comments="Automatically generated cardiomyocyte preparation from old dbnew; old prepid=%s" % oldprepid)
    prep_id=dbnew.query("SELECT id FROM prep_cardiomyocyte_mouse WHERE isolation=:iid",
                           iid=isol_id).first().id

    print("Cardiomyocyte preparation ID:", prep_id)
    Old2New[oldprepid] = prep_id

print(json.dumps(Old2New, sort_keys=True, indent=4, separators=(',', ': ')))
print()

with open("old2new.csv", "w") as f:
    for k in Old2New:
        f.write("%s,%d\n" % (k, Old2New[k]))
