from records.records import Database
import keyring, datetime, json

with Database("sqlite:///electrophysiology_ltcc.sqlite") as martin_sqlite:
    animalids = []
    for row in martin_sqlite.query("SELECT DISTINCT prep_id FROM experiments"):
        if row.prep_id.find(" ") < 0:
            animalids.append(int(row.prep_id))

    print("Looking for:", animalids)

username = "markov"
password = keyring.get_password("iocbio-kinetics", username)

database = Database("postgresql://" + username + ":" + password +
                    "@sysbio-db.kybi/experiments_v2")

A2PID={}
for animal in animalids:
    for row in database.query("SELECT * from animals_mouse WHERE id=:id",
                              id=animal):
        print('Animal:', row.id, row.death, row.comments)

        if database.query("SELECT COUNT(*) AS count FROM prep_animal_mouse WHERE animal=:id", id=animal).first().count == 0:
            print('Preparation animal missing, creating')
            database.query("INSERT INTO prep_animal_mouse(animal,ready,comments) VALUES(:id,:ready,:comments)",
                           id=animal, ready=datetime.datetime.combine(row.death, datetime.time(hour=9, minute=0)),
                           comments="Automatically generated preparation data")
        prep_animal_id = database.query("SELECT id FROM prep_animal_mouse WHERE animal=:id", id=animal).first().id

        if database.query("SELECT COUNT(*) AS count FROM prep_proc_isolation_cardio_mouse WHERE preparation_animal=:id",
                          id=prep_animal_id).first().count == 0:
            print('Cardiomyocytes isolation procedure missing, creating')
            database.query("INSERT INTO prep_proc_isolation_cardio_mouse(started,preparation_animal,comments) VALUES(:date,:pid,:comments)",
                           date=datetime.datetime.combine(row.death, datetime.time(hour=10, minute=0)),
                           pid=prep_animal_id, comments="Automatically generated isolation procedure data")
        isol_id = database.query("SELECT id FROM prep_proc_isolation_cardio_mouse WHERE preparation_animal=:id",
                                 id=prep_animal_id).first().id

        if database.query("SELECT COUNT(*) AS count FROM prep_cardiomyocyte_mouse WHERE isolation=:iid",
                          iid=isol_id).first().count == 0:
            print("Cardiomyocytes PREP ID is missing, creating")
            database.query("INSERT INTO prep_cardiomyocyte_mouse(isolation,ready,comments) VALUES(:iid,:date,:comments)",
                           iid=isol_id, date=datetime.datetime.combine(row.death, datetime.time(hour=12, minute=0)),
                           comments="Automatically generated cardiomyocyte preparation")
        prep_id=database.query("SELECT id FROM prep_cardiomyocyte_mouse WHERE isolation=:iid",
                               iid=isol_id).first().id

        print("Cardiomyocyte preparation ID:", prep_id)
        A2PID[animal] = prep_id

    print("")

print(json.dumps(A2PID, sort_keys=True, indent=4, separators=(',', ': ')))

for k in A2PID:
    print(k, A2PID[k])
