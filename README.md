# Example Web2py application

This is an application that was used for handling experimental data
in in Laboratory of Systems Biology, Department of Cybernetics, Tallinn 
University of Technology. At present, it is depricated and replaced by
[WebDB](https://gitlab.com/iocbio/webdb) based application. When compared to 
the WebDB solution, this application was much slower and imposed multiple
constraints on the database handling. However, if someone prefers to use 
Web2Py, we provide this code as an example.

## Copyright

Copyright (C) 2018 Laboratory of Systems Biology, Department of
Cybernetics, School of Science, Tallinn University of Technology
(https://sysbio.ioc.ee).

Software license: GPLv3, see [LICENSE](LICENSE).